//
//  Service.swift
//  SecureSaveFramework
//
//  Created by Ankit on 04/04/23.
//

import Foundation

public class Service {

    private init(){}

    public static func doSomething() -> String{
        return "Did some stuff, here you go homeboi!"
    }
}
